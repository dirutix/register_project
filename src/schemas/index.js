const Admin = require('./lib/Admin');
const Company = require('./lib/Company');
const Extract = require('./lib/Extract');
const User = require('./lib/User');

module.exports = {
    Admin,
    Company,
    Extract,
    User
}