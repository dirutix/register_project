const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-sequence')(mongoose);
const random = require('mongoose-random');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const extractSchema = new Schema({
    companyId: Number,
    companyName: String,
    date: Date,
    userId: Number,
    username: String,
    status: String
})

extractSchema.plugin(random, {
    path: 'r'
});
extractSchema.plugin(autoIncrement, {
    inc_field: 'extract_id'
});

const Extract = mongoose.model('extract', extractSchema);

async function createExtract(obj){
    return new Promise((resolve, reject) => {
        Extract.create(obj, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('created:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findAllByIds(ids){
    return new Promise((resolve, reject) => {
        Extract.find({extract_id: {$in: ids}}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND EXTRACTS:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findByUserId(id){
    return new Promise((resolve, reject) => {
        Extract.find({userId: id}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                // let nev = data.map((el, i, arr) => {
                //     let upDATEd = new Date(el.date);
                //     console.log(upDATEd.toDateString());
                //     el.date = upDATEd.toDateString();
                //     console.log(el);
                //     return el;
                // });
                console.log('FOUND EXTRACTS:\n' + data);
                resolve(data);
            }
        })
    })
}

async function setStatus(id, status){
    return new Promise((resolve, reject) => {
        Extract.updateOne({extract_id: id}, {$set: {status: status}}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND EXTRACTS:\n' + data);
                resolve(data);
            }
        })
    })
}

module.exports = {
    Extract,
    createExtract,
    findAllByIds,
    setStatus,
    findByUserId
}