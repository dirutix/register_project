const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-sequence')(mongoose);
const random = require('mongoose-random');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const adminSchema = new Schema({
    name: String,
    surname: String,
    role: String,
    username: String,
    hashPass: String,
    references: [Number]
})

adminSchema.plugin(random, {
    path: 'r'
});
adminSchema.plugin(autoIncrement, {
    inc_field: 'admin_id'
});

const Admin = mongoose.model('admin', adminSchema);

async function createAdmin(obj){
    return new Promise((resolve, reject) => {
        Admin.create(obj, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('created:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findByUsername(username){
    return new Promise((resolve, reject) => {
        Admin.findOne({username: username}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND ADMIN:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findById(id){
    return new Promise((resolve, reject) => {
        Admin.findOne({admin_id: id}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND ADMIN:\n' + data);
                resolve(data);
            }
        })
    })
}

async function addReferenceById(admin_id, ref_id){
    return new Promise((resolve, reject) => {
        Admin.update({admin_id}, {$push: {references: ref_id}}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('ADDED REFERENCE:\n' + data);
                resolve(data);
            }
        })
    })
}

module.exports = {
    Admin,
    createAdmin,
    findByUsername,
    findById,
    addReferenceById
}