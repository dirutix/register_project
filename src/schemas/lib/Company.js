const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-sequence')(mongoose);
const random = require('mongoose-random');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const companySchema = new Schema({
    adminId: Number,
    name: String,
    code: Number,
    adress: String,
    activityType: String,
    status: String
})

companySchema.plugin(random, {
    path: 'r'
});
companySchema.plugin(autoIncrement, {
    inc_field: 'company_id'
});

const Company = mongoose.model('company', companySchema);

async function createCompany(obj){
    return new Promise((resolve, reject) => {
        Company.create(obj, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('created:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findAll(){
    return new Promise((resolve, reject) => {
        Company.find({}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND COMPANIES:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findByName(name){
    return new Promise((resolve, reject) => {
        Company.find({name}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND COMPANIES:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findByCode(code){
    return new Promise((resolve, reject) => {
        Company.find({code}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND COMPANIES:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findById(id){
    return new Promise((resolve, reject) => {
        Company.findOne({company_id: id}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND COMPANY:\n' + data);
                resolve(data);
            }
        })
    })
}

async function update(obj){
    return new Promise((resolve, reject) => {
        Company.updateOne({company_id: obj.company_id}, {...obj}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('UPDATED COMPANY:\n' + data);
                resolve(data);
            }
        })
    })
}

module.exports = {
    Company,
    createCompany,
    findAll,
    findByName,
    findByCode,
    findById,
    update
}