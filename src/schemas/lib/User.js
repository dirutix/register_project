const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-sequence')(mongoose);
const random = require('mongoose-random');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const userSchema = new Schema({
    name: String,
    surname: String,
    username: String,
    email: String,
    hashPass: String
})

userSchema.plugin(random, {
    path: 'r'
});
userSchema.plugin(autoIncrement, {
    inc_field: 'user_id'
});

const User = mongoose.model('user', userSchema);

async function createUser(obj){
    return new Promise((resolve, reject) => {
        User.create(obj, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('CREATED:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findByUsernameOrEmail(username, email){
    return new Promise((resolve, reject) => {
        User.findOne({ $or: [{username}, {email}]}, (err, data) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND:\n' + data);
                resolve(data);
            }
        })
    })
}

async function findById(id){
    return new Promise((resolve, reject) => {
        User.findOne({user_id: id}, (err, data) => {
            if(err){
                console.log(err);
                reject(err);
            } else {
                console.log('FOUND USER');
                resolve(data);
            }
        })
    })
}

module.exports = {
    User,
    createUser,
    findByUsernameOrEmail,
    findById
}