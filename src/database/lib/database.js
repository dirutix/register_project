const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const config = require('../../config');


async function start(){
    await mongoose.connect(config.MONGO, {
        useNewUrlParser: true
    }, (err, data) => {
        if(err) console.log(err);
        else{
            console.log("mongo connected");
        }
    });
}

module.exports = {
    start: start
}