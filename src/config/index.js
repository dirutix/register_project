require('dotenv').config();
module.exports = {
    port: process.env.PORT || 8080,
    MONGO: process.env.MONGO,
    tokenKey: process.env.TOKEN_KEY,
    password: process.env.PASSWORD
};