const config = require('./config');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const jwtMiddleware = require('express-jwt');
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'dirutix@gmail.com',
        pass: config.password
    }
});
const app = express();
const bcrypt = require('bcrypt');
const Admin = require('./schemas').Admin;
const Company = require('./schemas').Company;
const Extract = require('./schemas').Extract;
const User = require('./schemas').User;
const path = require("path");
const database = require('./database').Database;

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(bodyParser.json());
app.use(cookieParser());

app.set('public', path.join(__dirname, '/../public'));
app.set('views', path.join(__dirname, '/views'));
app.use('/stylesheets', express.static(__dirname + '/public/stylesheets'));
app.use('/scripts', express.static(__dirname + '/public/scripts'));
app.set('view engine', 'ejs');

app.listen(config.port, (err) => {
    if (err) console.log(err);
    else {
        database.start()
            .then(() => {
                console.log("PORT: ", config.port);
            })
            .catch((err) => {
                console.log(err);
                return err;
            });
    };
});

//-------------------MIDDLEWARES--------------------------------------

app.use('', (req, res, next) => {
    if(!req.cookies.token){
        let token = jwt.sign({
            username: 'guest',
            role: 'guest'
        }, config.tokenKey);
        res.cookie('token', token);
        next();
    } else {
        next();
    }
})

app.use('/', jwtMiddleware({
    secret: config.tokenKey,
    getToken: function (req) {
        if (req.cookies && req.cookies.token) {
            // Handle token presented as a cookie parameter
            return req.cookies.token;
        }
        return jwt.sign({
            username: 'guest',
            role: 'guest'
        }, config.tokenKey);
    }
}))

//-----------------------------------------------------------

app.get('/', (req, res) => {
	res.redirect('');
});

app.get('/logout', (req, res) => {
    let token = jwt.sign({
        username: 'guest',
        role: 'guest'
    }, config.tokenKey);
    res.cookie('token', token);
    res.redirect('login');
})

//----------------AUTHENTICATION---------------------//

app.get('/login', (req, res) => {
    res.render('login', {
        user: req.user
    });
});

app.post('/login', (req, res) => {
    let {username, password, role} = req.body;
    if (role == 'user'){
        User.findByUsernameOrEmail(username, 'fake')
        .then(data => {
            if(data && bcrypt.compareSync(password, data.hashPass)){
                const token = jwt.sign({
                    username: username,
                    user_id: data.user_id,
                    role: 'user'
                }, config.tokenKey);
                res.cookie('token', token);
                res.redirect('search');
            } else {
                res.render('error', {
                    user: req.user,
                    message: "You're not a user"
                })
            }
        })
        .catch(err => console.log(err));
    } else if(role == 'admin'){
        Admin.findByUsername(username)
        .then(data => {
            if(data && bcrypt.compareSync(password, data.hashPass)){
                const token = jwt.sign({
                    username: username,
                    admin_id: data.admin_id,
                    role: 'admin'
                }, config.tokenKey);
                res.cookie('token', token);
                res.redirect('search');
            } else {
                res.render('error', {
                    user: req.user,
                    message: "You're not an admin"
                })
            }
        })
        .catch(err => console.log(err));
    } else {
        res.render({
            message: "Fake login request"
        })
    }
})

//----------------------------------------------------

//-----------------REGISTRATION---------------------//

app.get('/register', (req, res) => {
	res.render('register', {
        user: req.user
    });
});

app.post('/register', (req, res) => {
    console.log('Registration');
    let {name, surname, username, email, password} = req.body;
    let hashPass = bcrypt.hashSync(password, 10);
    User.findByUsernameOrEmail(username, email)
    .then(data => {
        if(!data){
            User.createUser({name, surname, username, email, hashPass})
            .then(data => {
                res.redirect('login');
            })
            .catch(err => console.log(err));
        } else {
            res.render('error', {
                user: req.user,
                message: "Username or email already exist"
            })
        }
    })
    .catch(err => console.log(err));
})

//--------------------------------------------------

//-----------------------SEARCH---------------------

app.get('/search', (req, res) => {
    let {request, option} = req.query;
    if(option == 'name'){
        Company.findByName(request)
        .then(data => {
            res.render('search', {
                user: req.user,
                data
            })
        })
        .catch(err => console.log(err));
    } else if(option == 'code'){
        Company.findByCode(request)
        .then(data => {
            res.render('search', {
                user: req.user,
                data
            })
        })
        .catch(err => console.log(err));
    } else {
        Company.findAll()
        .then(data => {
            res.render('search', {
                user: req.user,
                data
            })
        })
        .catch(err => console.log(err));
    }
});

//------------------------------------------------------------

//-----------------------PROFILES-----------------------------

app.get('/admin', (req, res) => {
    if(req.user.role == 'admin'){
        Admin.findById(req.user.admin_id)
        .then(data => {
            res.render('admin', {
                user: req.user,
                data
            })
        })
    }
});

app.get('/user/:id', (req, res) => {
    if(req.user.role == 'user' || req.user.role == 'admin'){
        User.findById(req.params.id)
        .then(data => {
            Extract.findByUserId(req.params.id)
            .then(extracts => {
                res.render('user', {
                    user: req.user,
                    data,
                    extracts
                })
            })
            .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
    }
});

//---------------------------------------------------------------

//---------------------COMPANY REGISTRATION-----------------------

app.get('/register_pb', (req, res) => {

    if(req.user.role != 'admin'){
        res.json({
            message: "You don't have access here" 
        });
    } else {
        res.render('register_pb', {
            user: req.user
        });
    }
});

app.post('/register_pb', (req, res) => {
    let {name, code, adress, activityType, status} = req.body;
    Company.createCompany({name, code, adress, activityType, status, adminId: req.user.admin_id})
    .then(dat => {
        Admin.findById(req.user.admin_id)
        .then(data => {
            res.render('admin', {
                user: req.user,
                data
            })
        })
    })
    .catch(err => console.log(err));
})

//-----------------------------------------------------------------

//---------------------COMPANY UPDATING-----------------------------

app.get('/update_pb/:id', (req, res) => {
    Company.findById(req.params.id)
    .then(data => {
        res.render('update', {
            user: req.user,
            data
        })
    })
})

app.post('/update_pb/:id', (req, res) => {
    let {name, code, adress, activityType, status} = req.body;
    Company.update({name, code, adress, activityType, status, adminId: req.user.admin_id, company_id: req.params.id})
    .then(data => {
        res.redirect('/pb/' + req.params.id);
    })
    .catch(err => console.log(err));
})

//---------------------------------------------------------

//---------------------EXTRACTIONS LOGIC-------------------

app.get('/issuance_list', (req, res) => {
    Admin.findById(req.user.admin_id)
    .then(admin => {
        Extract.findAllByIds(admin.references)
        .then(data => {
            res.render('issuance', {
                user: req.user,
                data
            })
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
});

app.get('/get_issuance', (req, res) => {
    Extract.createExtract({
        companyId: req.query.id,
        companyName: req.query.name,
        date: Date.now(),
        userId: req.user.user_id,
        username: req.user.username,
        status: 'Waiting'
    })
    .then(iss => {
        Admin.addReferenceById(req.query.admin_id, iss.extract_id)
        .then(data => {
            // res.json({
            //     message: "Wait for response on your email"
            // })
            res.redirect('search');
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err))
})

app.post('/send_extract/:id', (req, res) => {
    console.log(req.body.company);
    Company.findById(req.body.company)
    .then(company => {
        User.findById(req.body.user)
        .then(user => {
            console.log
            transporter.sendMail({
                from: 'dirutix@gmail.com',
                to: user.email,
                subject: 'Your extraction',
                html: ` <b> Довідка з єдиного реєстра підприємств щодо яких порушено провадження у справі про банкротство </b>
                <br/> <br/> <table class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">${company.name}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-light">
                        <td>Код:</td>
                        <td> ${company.code} </td>
                    </tr>
                    <tr class="table-light">
                        <td>Адреса:</td>
                        <td>  ${company.adress} </td>
                    </tr>
                    <tr class="table-light">
                        <td>Тип активiв :</td>
                        <td> ${company.activityType} </td>
                    </tr>
                    <tr class="table-light">
                        <td>Cтатус справи:</td>
                        <td> ${company.status} </td>
                    </tr>
                </tbody>
            </table>`
            });
            Extract.setStatus(req.params.id, "Sended")
            .then(data => {
                res.json({
                    message: "AAAAAAAAAAAAAAAAAA"
                })
            })
            .catch(err => console.log(err));
            res.redirect('/issuance_list');
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
})

app.get('/reject_extract/:id', (req, res) => {
    Extract.setStatus(req.params.id, "Rejected")
    .then(data => {
        res.redirect('/issuance_list');
    })
    .catch(err => console.log(err));
})

//-----------------------------------------------------------------

app.get('/pb/:id', (req, res) => {
    Company.findById(req.params.id)
    .then(data => {
        res.render('pb', {
            user: req.user,
            data
        })
    })
    .catch(err => console.log(err));
});